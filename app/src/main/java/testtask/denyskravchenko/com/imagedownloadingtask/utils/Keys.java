package testtask.denyskravchenko.com.imagedownloadingtask.utils;

/**
 * Created by Denys Kravchenko on 05.06.2015.
 */
public class Keys {
    public static final String DOWNLOAD_STATUS = "download_status";
    public static final String DOWNLOADED_BITMAP = "downloaded_bitmap";
    public static final String BITMAP_URL = "bitmap_url";
    public static final String TASK_ID = "task_id";
    public static final String ERROR_MESSAGE = "error_message";
    public static final String DOWNLOADING_PROGRESS = "progress";
    public static final int DEFAULT_INT_VALUE = -1;
}
