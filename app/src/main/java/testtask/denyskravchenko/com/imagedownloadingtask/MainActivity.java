package testtask.denyskravchenko.com.imagedownloadingtask;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.CopyOnWriteArrayList;

import testtask.denyskravchenko.com.imagedownloadingtask.service.Downloader;
import testtask.denyskravchenko.com.imagedownloadingtask.utils.Keys;
import testtask.denyskravchenko.com.imagedownloadingtask.utils.Utils;

/**
 * Created by Denys Kravchenko on 05.06.2015.
 */
public class MainActivity extends AppCompatActivity {

    private GalleryAdapter mGalleryAdapter;
    private Gallery mGallery;

    private int mBackClicksCounter;

    private boolean mIsReceiverRegistered;

    private DownloadReceiver mDownloadStatusReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        registerDownloadStatusReceiver();
        initGallery();
        final EditText editText = (EditText) findViewById(R.id.url);
        Button button = (Button) findViewById(R.id.start_download_task);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.hideKeyboard(MainActivity.this);
                if (!mIsReceiverRegistered) {
                    Toast.makeText(MainActivity.this, getString(R.string.service_disabled_message), Toast.LENGTH_SHORT).show();
                    return;
                }
                String url = editText.getText().toString();
                if (url.isEmpty()) {
                    Toast.makeText(MainActivity.this, getString(R.string.empty_url_error_message), Toast.LENGTH_SHORT).show();
                    return;
                }
                postJob(url);
//                postJob("http://edmullen.net/test/rc.jpg");
            }
        });
    }

    private void initGallery() {
        mGallery = (Gallery) findViewById(R.id.gallery);
        mGalleryAdapter = new GalleryAdapter();
        mGallery.setAdapter(mGalleryAdapter);
    }

    private void postJob(String url) {
        if (url == null) return;
        Intent intent = new Intent(this, Downloader.class);
        int taskId = intent.hashCode();
        intent.putExtra(Keys.BITMAP_URL, url);
        intent.putExtra(Keys.TASK_ID, taskId);
        startService(intent);
        mGalleryAdapter.showPlaceHolder(taskId, url);
        mGallery.setSelection(mGalleryAdapter.getCount() - 1, true);
    }

    private void stopDownloaderService() {
        Intent intent = new Intent(this, Downloader.class);
        stopService(intent);
        unregisterDownloadStatusReceiver();
    }

    private void startDownloaderService() {
        Intent intent = new Intent(this, Downloader.class);
        startService(intent);
        registerDownloadStatusReceiver();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.disable_downloader) {
            stopDownloaderService();
            return true;
        } else if (id == R.id.enable_downloader) {
            startDownloaderService();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void registerDownloadStatusReceiver() {
        if (mIsReceiverRegistered) return;
        IntentFilter intentFilter = new IntentFilter(
                Keys.DOWNLOAD_STATUS);
        mDownloadStatusReceiver =
                new DownloadReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(
                mDownloadStatusReceiver,
                intentFilter);
        mIsReceiverRegistered = true;
    }

    private void unregisterDownloadStatusReceiver() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(
                mDownloadStatusReceiver);
        mIsReceiverRegistered = false;
    }

    private class DownloadReceiver extends BroadcastReceiver {
        private DownloadReceiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            String exceptionMessage = (String) intent.getStringExtra(Keys.ERROR_MESSAGE);
            String url = (String) intent.getStringExtra(Keys.BITMAP_URL);
            Bitmap bitmap = (Bitmap) intent.getParcelableExtra(Keys.DOWNLOADED_BITMAP);
            int taskId = intent.getIntExtra(Keys.TASK_ID, Keys.DEFAULT_INT_VALUE);
            int progress = intent.getIntExtra(Keys.DOWNLOADING_PROGRESS, Keys.DEFAULT_INT_VALUE);

            if (exceptionMessage != null && taskId != Keys.DEFAULT_INT_VALUE) {
                String baseMessage = getString(R.string.error_message);
                mGalleryAdapter.postError(taskId, baseMessage);
                Toast.makeText(MainActivity.this, baseMessage, Toast.LENGTH_SHORT).show();
            }

            if (progress != Keys.DEFAULT_INT_VALUE && taskId != Keys.DEFAULT_INT_VALUE) {
                mGalleryAdapter.postProgress(taskId, progress);
            }
            if (bitmap != null && taskId != Keys.DEFAULT_INT_VALUE) {
                mGalleryAdapter.addImage(taskId, bitmap);
            }
        }
    }

    private class DownloadEntity {
        private String mErrorMessage;
        private int mTaskId;
        private String mUrl;
        private Bitmap mBitmap;
        private int mProgress;

        public DownloadEntity(String mUrl, Bitmap mBitmap, int mProgress) {
            this.mUrl = mUrl;
            this.mBitmap = mBitmap;
            this.mProgress = mProgress;
        }

        public DownloadEntity(int taskId, Bitmap mBitmap) {
            this.mTaskId = taskId;
            this.mBitmap = mBitmap;
        }

        public DownloadEntity(int taskId, String url, Bitmap mBitmap) {
            this.mUrl = url;
            this.mTaskId = taskId;
            this.mBitmap = mBitmap;
        }

        public boolean equals(Object o) {
            if (o == null) return false;
            if (!(o instanceof DownloadEntity)) return false;

            DownloadEntity other = (DownloadEntity) o;

            return this.mTaskId == other.mTaskId;
        }

        public int hashCode() {
            return (int) mUrl.hashCode();
        }
    }

    class GalleryAdapter extends BaseAdapter {
        private CopyOnWriteArrayList<DownloadEntity> mItems;

        private GalleryAdapter() {
            mItems = new CopyOnWriteArrayList<>();
        }

        public int getCount() {
            return mItems.size();
        }

        public void addImage(int taskId, Bitmap bitmap) {
            for (DownloadEntity downloadEntity : mItems) {
                if (mItems.remove(downloadEntity)) {
                    if (mItems.add(new DownloadEntity(taskId, bitmap))) {
                        notifyDataSetChanged();
                    }
                }
            }
        }

        public void postProgress(int taskId, int progress) {
            for (DownloadEntity downloadEntity : mItems) {
                if (downloadEntity.mTaskId == taskId) {
                    downloadEntity.mProgress = progress;
                    notifyDataSetChanged();
                }
            }
        }

        public void postError(int taskId, String errorMessage) {
            for (DownloadEntity downloadEntity : mItems) {
                if (downloadEntity.mTaskId == taskId) {
                    downloadEntity.mErrorMessage = errorMessage;
                    notifyDataSetChanged();
                }
            }
        }

        public void showPlaceHolder(int taskId, String url) {
            mItems.add(new DownloadEntity(taskId, url, null));
            notifyDataSetChanged();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        class ViewHolder {
            ImageView imageView;
            TextView message;
            TextView error;
            TextView progressIndicator;
            RelativeLayout placeHolderContainer;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            Activity context = MainActivity.this;
            ViewHolder holder;
            if (convertView == null) {
                LayoutInflater inflater = context.getLayoutInflater();
                convertView = inflater.inflate(R.layout.gallery_item, null, true);
                holder = new ViewHolder();
                holder.imageView = (ImageView) convertView.findViewById(R.id.imageView);
                holder.message = (TextView) convertView.findViewById(R.id.url);
                holder.error = (TextView) convertView.findViewById(R.id.error_message);
                holder.progressIndicator = (TextView) convertView.findViewById(R.id.progress_value);
                holder.placeHolderContainer = (RelativeLayout) convertView.findViewById(R.id.placeholder);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            String errorMessage = mItems.get(position).mErrorMessage;
            Bitmap bitmap = mItems.get(position).mBitmap;
            String url = mItems.get(position).mUrl;
            int progress = mItems.get(position).mProgress;
            if (errorMessage != null) {
                holder.placeHolderContainer.setVisibility(View.GONE);
                holder.imageView.setVisibility(View.GONE);
                holder.error.setText(errorMessage);
                holder.error.setVisibility(View.GONE);
                holder.message.setVisibility(View.GONE);
            } else if (bitmap == null) {
                holder.placeHolderContainer.setVisibility(View.VISIBLE);
                holder.imageView.setVisibility(View.GONE);
                if (progress != Keys.DEFAULT_INT_VALUE)
                    holder.progressIndicator.setText(Integer.toString(progress) + getString(R.string.progress_indicator));
                holder.message.setText(url);
                holder.message.setVisibility(View.VISIBLE);
                ;
                holder.error.setVisibility(View.GONE);
            } else {
                holder.placeHolderContainer.setVisibility(View.GONE);
                holder.imageView.setVisibility(View.VISIBLE);
                holder.imageView.setImageBitmap(bitmap);
                holder.error.setVisibility(View.GONE);
            }

            return convertView;
        }
    }

    public void onBackPressed() {
        mBackClicksCounter++;
        if (mBackClicksCounter == 2) {
            stopDownloaderService();
            super.onBackPressed();
        }
    }
}
