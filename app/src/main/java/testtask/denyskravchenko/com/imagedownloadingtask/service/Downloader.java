package testtask.denyskravchenko.com.imagedownloadingtask.service;

import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import testtask.denyskravchenko.com.imagedownloadingtask.utils.IDownloadStatusListener;
import testtask.denyskravchenko.com.imagedownloadingtask.utils.Keys;
import testtask.denyskravchenko.com.imagedownloadingtask.utils.Utils;

/**
 * Created by Denys Kravchenko on 05.06.2015.
 */
public class Downloader extends Service {
    private ExecutorService mExecutor;

    public Downloader() {
    }

    private void postError(int taskId, String errorMessage) {
        Intent intent =
                new Intent(Keys.DOWNLOAD_STATUS);
        intent.putExtra(Keys.ERROR_MESSAGE, errorMessage);
        intent.putExtra(Keys.TASK_ID, taskId);

        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void postProgress(int taskId, int progress) {
        Intent intent =
                new Intent(Keys.DOWNLOAD_STATUS);
        intent.putExtra(Keys.DOWNLOADING_PROGRESS, progress);
        intent.putExtra(Keys.TASK_ID, taskId);

        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void postResult(int taskId, Bitmap bitmap) {
        if (bitmap == null) return;
        Intent intent =
                new Intent(Keys.DOWNLOAD_STATUS);
        intent.putExtra(Keys.DOWNLOADED_BITMAP, bitmap);
        intent.putExtra(Keys.TASK_ID, taskId);

        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void downloadImage(final String URL, final int taskId) {
        if (URL == null) return;
        if (mExecutor == null) {
            int numCores = Runtime.getRuntime().availableProcessors();
            mExecutor = Executors.newFixedThreadPool(numCores);
        }
        mExecutor.submit(new Runnable() {
            @Override
            public void run() {
                Bitmap bitmap = null;
                try {
                    bitmap = Utils.downloadImage(URL, Downloader.this, new IDownloadStatusListener() {
                        @Override
                        public void publishProgress(int progress) {
                            postProgress(taskId, progress);
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                    //TODO Show diff messages according error type
                    postError(taskId, e.getMessage());
                    return;
                }
                bitmap = Utils.resizeBitmap(bitmap, (int) Utils.convertDpToPixel(320, Downloader.this), (int) Utils.convertDpToPixel(240, Downloader.this));
                if (bitmap != null) {
                    postResult(taskId, bitmap);
                }
            }
        });
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String url = intent.getStringExtra(Keys.BITMAP_URL);
        int taskId = intent.getIntExtra(Keys.TASK_ID, 0);
        Log.e("Downloader", "onStartCommand , url: " + url);
        downloadImage(url, taskId);
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onDestroy() {
        Log.e("Downloader", "onDestroy");
        if (mExecutor != null)
            mExecutor.shutdownNow();
    }
}
