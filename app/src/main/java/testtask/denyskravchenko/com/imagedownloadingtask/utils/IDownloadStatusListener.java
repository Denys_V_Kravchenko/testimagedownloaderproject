package testtask.denyskravchenko.com.imagedownloadingtask.utils;

/**
 * Created by Denys Kravchenko on 09.06.2015.
 */
public interface IDownloadStatusListener {

    void publishProgress(int progress);
}
