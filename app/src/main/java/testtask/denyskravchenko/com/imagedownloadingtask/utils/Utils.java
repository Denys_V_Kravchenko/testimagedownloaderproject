package testtask.denyskravchenko.com.imagedownloadingtask.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.DisplayMetrics;
import android.view.inputmethod.InputMethodManager;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Denys Kravchenko on 05.06.2015.
 */
public class Utils {

    private Utils() {
        throw new AssertionError("Non instantiable class");
    }

    public static void hideKeyboard(Activity activity) {
        try {
            InputMethodManager imm = (InputMethodManager)
                    activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception ignored) {
        }
    }

    public static Bitmap downloadImage(String urlString, Context context, IDownloadStatusListener listener) throws IOException {
        URL url = new URL(urlString);
        String filePath = context.getCacheDir() + File.separator + urlString.hashCode();
        URLConnection connection = url.openConnection();
        connection.connect();

        int lenghtOfFile = connection.getContentLength();
        InputStream input = new BufferedInputStream(url.openStream());
        OutputStream output = new FileOutputStream(filePath);

        byte data[] = new byte[1024];
        long total = 0;
        int count = 0;
        int status = 0;
        while ((count = input.read(data)) != -1) {
            total += count;
            status = (int) ((total * 100) / lenghtOfFile);
            output.write(data, 0, count);
            if (listener != null)
                listener.publishProgress(status);
        }

        output.flush();
        output.close();
        input.close();

        Bitmap bitmap = BitmapFactory.decodeFile(filePath);

        return bitmap;
    }

    public static Bitmap resizeBitmap(Bitmap bitmap, int newHeight, int newWidth) {

        return Bitmap.createScaledBitmap(bitmap, newWidth, newHeight,
                true);
    }

    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }
}
